angular.module('airline', ['ngRoute', 'airlineServices', 'ngCookies'])
	.config(airlineRouter);

function airlineRouter($routeProvider){
	$routeProvider
		.when('/airports', {
			templateUrl: 'partials/airports.html',
			controller: 'AirportsCtrl'
		})
		.when('/airports/:airportCode', {
			templateUrl: 'partials/airport.html',
			controller: 'AirportCtrl'
		})
		.when('/airports/:airport1/:airport2', {
			templateUrl: 'partials/two_airports.html',
			controller: 'TwoAirportsCtrl'
		})
		.when('/flights', {
			templateUrl: 'partials/flights.html',
			controller: 'FlightsCtrl'
		})
		.when('/flights/:flightNumber', {
			templateUrl: 'partials/flight.html',
			controller: 'FlightCtrl'
		})
		.when('/reservations', {
			templateUrl: 'partials/reservations.html',
			controller: 'ReservationsCtrl'
		})
		.when('/reservations/:id', {
			templateUrl: 'partials/reservation.html',
			controller: 'ReservationCtrl'
		})
		.when('/register', {
			templateUrl: 'partials/register.html',
			controller: 'RegisterCtrl'
		})
		.when('/login', {
			templateUrl: 'partials/login.html',
			controller: 'LoginCtrl'
		})
		.otherwise({
			redirectTo: '/login'
		});
};