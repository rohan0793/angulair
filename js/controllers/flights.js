function FlightsCtrl($scope, Flight, $location){

	if(!$scope.isLoggedIn){
		$location.path('login');
	}else{
		$scope.setActive('flights');
		$scope.flights = Flight.query();
	}

	
}