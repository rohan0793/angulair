function ReservationsCtrl($scope, Reservation, Flight, Airport, $location, Auth){

	if(!$scope.isLoggedIn){
		$location.path('login');
	}else{
		$scope.setActive('reservations');

		$scope.reservations = Reservation.query();
		$scope.flights = Flight.query();
		$scope.airports = Airport.query();

		$scope.reserveFlight = function (flight) {
			// Fetching the authenticated user
			Auth.get(function(user){
				// Compiling details of reservation
				reservationData = {
					'flight_id': flight.id,
					'user_id': user.id
				};
				// Saving reservation to server
				Reservation.save(
					reservationData, 
					function(data){
						// Performing the required changes on the front end if
						// save request was successful on the server
						$scope.reserve.origin = '';
						$scope.reserve.destination = '';
						$scope.reservations.push(data);
					}
				);
			});
			
		};

		$scope.cancelFlight = function (reservationId){
			Reservation.remove({id: reservationId}, function(){
				$scope.reservations = $scope.reservations.filter(function (el) {
					return el.id !== reservationId;
				});
			});
		};
	}

	
}