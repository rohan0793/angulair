function ReservationCtrl($scope, $routeParams, Reservation, $location){
	if(!$scope.isLoggedIn){
		$location.path('login');
	}else{
		$scope.currentReservation = Reservation.get({
			id: $routeParams.id
		});

		$scope.setActive('reservations');
	}
}