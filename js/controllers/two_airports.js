function TwoAirportsCtrl($scope, $routeParams, Airport){
	$scope.airport1 = Airport.get({
		airportCode: $routeParams.airport1
	});
	$scope.airport2 = Airport.get({
		airportCode: $routeParams.airport2
	});
}