function DestinationsCtrl($scope, Airport, $location){

	if(!$scope.isLoggedIn){
		$location.path('login');
	}else{
		$scope.setActive('destinations');
		$scope.sidebarURL = 'partials/airport.html';

		$scope.currentAirport = null;

		$scope.setAirport = function (code) {
			$scope.currentAirport = Airport.get({
				airportCode: code
			});
		};

		$scope.airports = Airport.query();
	}

	
}