function AppCtrl ($scope, $cookieStore, $location, Auth, User, $http) {
  $scope.setActive = function (type) {
    $scope.airportsActive = '';
    $scope.flightsActive = '';
    $scope.reservationsActive = '';

    $scope[type + 'Active'] = 'active';
  };

  $scope.authenticate = function (credentials) {
    Auth.save(credentials, function(data){
      $cookieStore.put('auth_token', data.auth_token);
      $scope.isLoggedIn = true;
      $location.path('airports');
      $scope.message = null;
    }, function(response){
        if(response.status === 401){
          $scope.message = "Email/Password combination incorrect!";
        }
        if(response.status > 401 && response.status < 500){
          $scope.message = "You cannot access the application. Error: " + response.status;
        }
        
    });
  };

  $scope.logout = function () {
    Auth.delete(
      {}, // Empty parameter object
      {}, // Empty post data object
      function(data){
        $location.path('/login');
        $scope.isLoggedIn = false;
        $cookieStore.remove('auth_token');
      }
    );  
  };

  $scope.register = function(credentials){
    User.save(
      // Passing data
      credentials,
      function(data){
        // Registeration success. Log user in.
        $scope.authenticate(credentials);
      },
      function(data, status, headers, config){
        // Registeration fail. Show errors
        $scope.messages = data.data;
      }
    );
  };
  
  if($cookieStore.get('auth_token')){
    $scope.isLoggedIn = true;
  }else{
    $scope.isLoggedIn = false;
  }

}