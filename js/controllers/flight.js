function FlightCtrl($scope, $routeParams, Flight, $location){
	if(!$scope.isLoggedIn){
		$location.path('login');
	}else{
		$scope.currentFlight = Flight.get({
			flightNumber: $routeParams.flightNumber
		});

		$scope.setActive('flights');
	}
}