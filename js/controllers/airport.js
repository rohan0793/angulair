function AirportCtrl($scope, $routeParams, Airport, $location){
	if(!$scope.isLoggedIn){
		$location.path('login');
	}else{
		$scope.currentAirport = Airport.get({
			airportCode: $routeParams.airportCode
		});

		$scope.setActive('airports');
	}
}