function AirportsCtrl($scope, Airport, $location, Flight, $http, $cookieStore){

	if(!$scope.isLoggedIn){
		$location.path('login');
	}else{
		$http.defaults.headers.common.Authorization = $cookieStore.get('auth_token');
		$scope.setActive('airports');
		$scope.sidebarURL = 'partials/airport.html';

		$scope.currentAirport = null;

		$scope.setAirport = function (code) {
			$scope.currentAirport = Airport.get({
				airportCode: code
			});
		};

		$scope.airports = Airport.query();
	}

	
}