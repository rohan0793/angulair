angular.module('airlineServices', ['ngResource', 'ngCookies'])
	.service('Url', function($location){
		if($location.host() === 'localhost'){
			this.url =  'localhost:8000';
		}else{
			this.url = 'angulairapi.rohanchhabra.in';
		}
	})
	.factory('Airport', function($resource, $cookieStore, Url){
		return $resource("http://" + Url.url + "/airports/:airportCode", {}, {
			query: { method: "GET", isArray: true }
		});
	})
	.factory('Flight', function($resource, $cookieStore, Url){
		return $resource("http://" + Url.url +"/flights/:flightNumber");
	})
	.factory('Reservation', function($resource, $cookieStore, Url){
		return $resource("http://" + Url.url +"/reservations/:id");
	})
	.factory('Auth', function($resource, $cookieStore, Url){
		return $resource("http://" + Url.url +"/auth");
	})
	.factory('User', function($resource, $cookieStore, Url){
		return $resource("http://" + Url.url +"/users");
	});